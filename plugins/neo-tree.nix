{ ... }:
{
  programs.nixvim.plugins.neo-tree = {
    enable = true;

    sources = [ "filesystem" "buffers" "git_status" "document_symbols" ];

    # open_files_do_not_replace_types = { "terminal", "Trouble", "trouble", "qf", "Outline" },

    filesystem = {
      bindToCwd = false;
      useLibuvFileWatcher = true;
    };
    buffers.followCurrentFile.enabled = true;

    window.mappings = {
      "<space>" = "none";
      "Y".__raw = ''function(state)
        local node = state.tree:get_node()
        local path = node:get_id()
        vim.fn.setreg("+", path, "c")
      end'';
    };

    defaultComponentConfigs.indent = {
      withExpanders = true; # if nil and file nesting is enabled, will enable expanders
      expanderCollapsed = "";
      expanderExpanded = "";
      expanderHighlight = "NeoTreeExpander";
    };
  };
}
