{ ... }:
{
  programs.nixvim.plugins.telescope = {
    enable = true;

    settings.defaults = {
      prompt_prefix = " ";
      selection_caret = " ";
      # open files in the first window that is an actual file.
      # use the current window if no other window is available.
      get_selection_window =
        ''function()
          local wins = vim.api.nvim_list_wins()
          table.insert(wins, 1, vim.api.nvim_get_current_win())
          for _, win in ipairs(wins) do
            local buf = vim.api.nvim_win_get_buf(win)
            if vim.bo[buf].buftype == "" then
              return win
            end
          end
          return 0
        end,'';
    };

    extensions = {
      fzf-native.enable = true;
      file-browser.enable = true;
    };

/*
    # TODO
      keymaps = {
        "<c-t>" = ''function(...)
                    return require("trouble.providers.telescope").open_with_trouble(...)
		  end'';
        "<a-t>" = ''function(...)
                    require("trouble.providers.telescope").open_selected_with_trouble(...)
	  	  end'';
        "<a-i>" = ''function()
                    local action_state = require("telescope.actions.state")
      		    local line = action_state.get_current_line()
                    Util.telescope("find_files", { no_ignore = true, default_text = line })()
                  end,'';
        "<a-h>" = ''function()
                    local action_state = require("telescope.actions.state")
                    local line = action_state.get_current_line()
                    Util.telescope("find_files", { hidden = true, default_text = line })()
                  end,'';
        "<C-Down>" = "require("telescope.actions").cycle_history_next";
        "<C-Up>" = "require("telescope.actions").cycle_history_prev";
        "<C-f>" = "require("telescope.actions").preview_scrolling_down";
        "<C-b>" = "require("telescope.actions").preview_scrolling_up";
        "q" = "require("telescope.actions").close";
      };
*/
  };
}
