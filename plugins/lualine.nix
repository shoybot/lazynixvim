{ ... }:
{
  programs.nixvim.plugins.lualine = {
    enable = true;

    theme = "auto";
    disabledFiletypes.statusline = [ "dashboard" "alpha" "starter" ];
    globalstatus = true;

    sections = {
      lualine_a = [ "mode" ];
      lualine_b = [ "branch" ];
      lualine_c = [
        ''Util.lualine.root_dir()''
        {
          name = "diagnostics";
          icons_enabled = true;
        }
        {
          name = "filetype";
          icons_enabled = true;
          padding = { left = 1; right = 0; };
        }
        ''Util.lualine.pretty_path()''
      ];
      lualine_x = [
          # stylua: ignore
          ''
            function() return require("noice").api.status.command.get() end,
            cond = function() return package.loaded [ "noice" ] and require ("noice").api.status.command.has () end,
            color = Util.ui.fg("Statement"),
          ''
          # stylua: ignore
          ''
            function() return require("noice").api.status.mode.get() end,
            cond = function() return package.loaded [ "noice" ] and require ("noice").api.status.mode.has () end,
            color = Util.ui.fg("Constant")
          ''
          # stylua: ignore
          ''
            function() return "  " .. require("dap").status() end,
            cond = function() return package.loaded [ "dap" ] and require ("dap").status () ~= "" end,
            color = Util.ui.fg("Debug")
          ''
          #{
          #name = ''require("lazy.status").updates'';
          #cond = ''require ("lazy.status").has_updates'';
          #color = ''Util.ui.fg("Special")'';
          #}
          ''
            "diff",
            source = function()
              local gitsigns = vim.b.gitsigns_status_dict
              if gitsigns then
                return {
                  added = gitsigns.added,
                  modified = gitsigns.changed,
                  removed = gitsigns.removed,
                }
              end
            end
          ''
      ];
      lualine_y = [
        {
          name = "progress";
          separator = { left = " "; right = " "; };
          padding = { left = 1; right = 0; };
        }
        {
          name = "location";
          padding = { left = 0; right = 1; };
        }
      ];
      lualine_z = [
        # Doesn't work
        /*
        ''function()
          return " " .. os.date("%R")
        end''
        */
      ];
    };

    extensions = [ "neo-tree" /*"lazy"*/ ];
  };
}
