{ config, lib, pkgs, inputs, ... }:
{
  imports = [
    ./bufferline.nix 
    ./cmp.nix
    ./conform-nvim.nix
    ./dashboard.nix
    ./gitsigns.nix
    ./lsp.nix
    ./lualine.nix
    ./mini.nix
    ./neo-tree.nix
    ./telescope.nix
    ./tree-sitter.nix
    ./which-key.nix
  ];
  programs.nixvim = {
    plugins = {
      # Coding
      luasnip = {
        enable = true;
        extraConfig = {
          history = true;
          delete_check_events = "TextChanged";
        };
      };
      # Editing
      trouble = {
        enable = true;
        settings.use_diagnostics_signs = true;
      };

      flash.enable = true;

      illuminate = {
        enable = true;
        delay = 200;
        largeFileCutoff = 2000;
        largeFileOverrides.providers = [ "lsp" ];
      };

      todo-comments.enable = true;
      # Linting
      lint = {
        enable = true;

        lintersByFt = {
          "*" = [ "global linter" ];
          #fish = [ "fish" ];
          # Use the "*" filetype to run linters on all filetypes.
          # ['*'] = { 'global linter' },
          # Use the "_" filetype to run linters on filetypes that don't have other linters configured.
          #  ['_'] = { 'fallback linter' },
        };
      };
      # UI
      # alpha.enable = true; #nil
      notify = {
        enable = true;
        timeout = 3000;
        maxHeight.__raw = ''function() return math.floor(vim.o.lines * 0.75) end'';
        maxWidth.__raw = ''function() return math.floor(vim.o.columns * 0.75) end'';
        onOpen = ''function(win) vim.api.nvim_win_set_config(win, { zindex = 100 }) end'';
      };

      indent-blankline = {
        enable = true;
        settings = {
          indent = {
            char = "│";
            tab_char = "│";
          };
          scope.enabled = true;
          exclude.filetypes = [
            "help"
            "alpha"
            "dashboard"
            "neo-tree"
            "Trouble"
            "trouble"
            "lazy"
            "mason"
            "notify"
            "toggleterm"
            "lazyterm"
          ];
        };
      };
      # LSP
      noice = {
        enable = true;
        lsp.override = {
            "vim.lsp.util.convert_input_to_markdown_lines" = true;
            "vim.lsp.util.stylize_markdown" = true;
            "cmp.entry.get_documentation" = true;
        };
        routes = [
          {
            filter = {
              event = "msg_show";
              any = [
                { find = "%d+L, %d+B"; }
                { find = "; after #%d+"; }
                { find = "; before #%d+"; }
              ];
            };
            view = "mini";
          }
        ];
        presets = {
          bottom_search = true;
          command_palette = true;
          long_message_to_split = true;
          inc_rename = true;
        };
      };
      # Utility
      persistence.enable = true;
      codeium-nvim.enable = true;
      codeium-vim.enable = true;

      lazy = {
        enable = true;
      };
    };

    extraPlugins = with pkgs.vimPlugins; [
      LazyVim
      # UI
      dressing-nvim #nil
      nvim-web-devicons #nil
      nui-nvim #nil
      friendly-snippets
      # Editor
      nvim-spectre # opts = { open_cmd = "noswapfile vnew" }
      telescope-fzf-native-nvim
      # LSP
      #mason-nvim # opts = { ensure_installed = { "stylua", "shfmt", -- "flake8", }, }
      #mason-lspconfig-nvim
      #nvim-lspconfig
      neoconf-nvim
      neodev-nvim
      # Utility
      vim-startuptime
      plenary-nvim
    ];
  };
}


