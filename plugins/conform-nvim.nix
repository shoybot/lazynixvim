{ ... }:
{
  programs.nixvim.plugins.conform-nvim = {
    enable = true;
    formattersByFt = {
      nix = [ "nixfmt" ];
      javascript = [ "prettier" ];
    };
    formatters = {
      injected.options.ignore_errors = true;
    };

    # TODO
    /*
      format = {
      timeout_ms = 3000,
      async = false, -- not recommended to change
      quiet = false, -- not recommended to change
      },
    */
  };
}
