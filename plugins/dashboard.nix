{ ... }:
{
  programs.nixvim.plugins.dashboard =
    let
      logo = ''string.rep("\n",8) .. [[
     ██╗      █████╗ ███████╗██╗   ██╗██╗   ██╗██╗███╗   ███╗          Z
     ██║     ██╔══██╗╚══███╔╝╚██╗ ██╔╝██║   ██║██║████╗ ████║      Z    
     ██║     ███████║  ███╔╝  ╚████╔╝ ██║   ██║██║██╔████╔██║   z       
     ██║     ██╔══██║ ███╔╝    ╚██╔╝  ╚██╗ ██╔╝██║██║╚██╔╝██║ z         
     ███████╗██║  ██║███████╗   ██║    ╚████╔╝ ██║██║ ╚═╝ ██║           
     ╚══════╝╚═╝  ╚═╝╚══════╝   ╚═╝     ╚═══╝  ╚═╝╚═╝     ╚═╝           
]] .. "\n\n"'';
    in
    {
      enable = true;

      hideStatusline = true;

      header = [ ''vim.split(${logo}, "\n")'' ];

      center = [
        {
          action = "Telescope find_files";
          desc = " Find file";
          icon = " ";
          shortcut = "f";
        }
        {
          action = "ene | startinsert";
          desc = " New file";
          icon = " ";
          shortcut = "n";
        }
        {
          action = "Telescope oldfiles";
          desc = " Recent files";
          icon = " ";
          shortcut = "r";
        }
        {
          action = "Telescope live_grep";
          desc = " Find text";
          icon = " ";
          shortcut = "g";
        }
        #{ action = [[lua require("lazyvim.util").telescope.config_files()()]], desc = " Config",          icon = " ", key = "c" },
        {
          action = ''lua require("persistence").load()'';
          desc = " Restore Session";
          icon = " ";
          shortcut = "s";
        }
        {
          action = "LazyExtras";
          desc = " Lazy Extras";
          icon = " ";
          shortcut = "x";
        }
        {
          action = "Lazy";
          desc = " Lazy";
          icon = "󰒲 ";
          shortcut = "l";
        }
        {
          action = "qa";
          desc = " Quit";
          icon = " ";
          shortcut = "q";
        }

      ];

      footer = [
        ''function()
        local stats = require("lazy").stats()
        local ms = (math.floor(stats.startuptime * 100 + 0.5) / 100)
        return { "⚡ Neovim loaded " .. stats.loaded .. "/" .. stats.count .. " plugins in " .. ms .. "ms" }
    end''
      ];
    };
}
