{ ... }:
{
  programs.nixvim.plugins = {
    treesitter = {
      enable = true;

      indent = true;

      ensureInstalled = [
        "bash"
        "c"
        "diff"
        "html"
        "javascript"
        "jsdoc"
        "json"
        "jsonc"
        "lua"
        "luadoc"
        "luap"
        "markdown"
        "markdown_inline"
        "python"
        "query"
        "regex"
        "toml"
        "tsx"
        "typescript"
        "vim"
        "vimdoc"
        "yaml"
      ];

      incrementalSelection = {
        enable = true;

        keymaps = {
          initSelection = "<C-space>";
          nodeIncremental = "<C-space>";
          scopeIncremental = "false";
          nodeDecremental = "<bs>";
        };
      };
    };
    treesitter-textobjects = {
      enable = true;

      move = {
        enable = true;

        gotoNextStart = {
          "]f".query = "@function.outer";
          "]c".query = "@class.outer";
        };
        gotoNextEnd = {
          "]F".query = "@function.outer";
          "]C".query = "@class.outer";
        };
        gotoPreviousStart = {
          "[f".query = "@function.outer";
          "[c".query = "@class.outer";
        };
        gotoPreviousEnd = {
          "[F".query = "@function.outer";
          "[C".query = "@class.outer";
        };
      };
    };
    treesitter-context = {
      enable = true;
      settings = {
        mode = "cursor";
        maxLines = 3;
      };
    };
    ts-autotag.enable = true;
    ts-context-commentstring = {
      disableAutoInitialization = true;
    };
  };
}
