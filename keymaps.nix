{ config, helpers, lib, pkgs, inputs, ... }:
{
  programs.nixvim.keymaps = [
    # Better up/down
    #map({ "n", "x" }, "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
    #map({ "n", "x" }, "<Down>", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
    #map({ "n", "x" }, "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
    #map({ "n", "x" }, "<Up>", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
    # Move to window using the <ctrl> hjkl keys
    { mode = [ "n" ]; action = "<C-w>h"; key = "<C-h>"; options = { desc = "Go to left window"; remap = true; }; }
    { mode = [ "n" ]; action = "<C-w>j"; key = "<C-j>"; options = { desc = "Go to left window"; remap = true; }; }
    { mode = [ "n" ]; action = "<C-w>k"; key = "<C-k>"; options = { desc = "Go to left window"; remap = true; }; }
    { mode = [ "n" ]; action = "<C-w>l"; key = "<C-l>"; options = { desc = "Go to left window"; remap = true; }; }
    # Resize window using <ctrl> arrow keys.
    { mode = [ "n" ]; key = "<C-Up>"; action = "<cmd>resize +2<cr>"; options = { desc = "Increase window height"; }; }
    { mode = [ "n" ]; key = "<C-Down>"; action = "<cmd>resize -2<cr>"; options = { desc = "Decrease window height"; }; }
    { mode = [ "n" ]; key = "<C-Left>"; action = "<cmd>vertical resize -2<cr>"; options = { desc = "Decrease window height"; }; }
    { mode = [ "n" ]; key = "<C-Right>"; action = ":vertical resize +2<cr>"; options = { desc = "Increase window height"; }; }
    # Move Lines
    { mode = "n"; key = "<A-j>"; action = "<cmd>m .+1<cr>=="; options = { desc = "Move down"; }; }
    { mode = "n"; key = "<A-k>"; action = "<cmd>m .-2<cr>=="; options = { desc = "Move up"; }; }
    { mode = "i"; key = "<A-j>"; action = "<esc><cmd>m .+1<cr>==gi"; options = { desc = "Move down"; }; }
    { mode = "i"; key = "<A-k>"; action = "<esc><cmd>m .-2<cr>==gi"; options = { desc = "Move up"; }; }
    { mode = "v"; key = "<A-j>"; action = ":m '>+1<cr>gv=gv"; options = { desc = "Move down"; }; }
    { mode = "v"; key = "<A-k>"; action = ":m '<-2<cr>gv=gv"; options = { desc = "Move up"; }; }
    # Buffers
    { mode = "n"; key = "<S-h>"; action = "<cmd>bprevious<cr>"; options = { desc = "Prev buffer"; }; }
    { mode = "n"; key = "<S-l>"; action = "<cmd>bnext<cr>"; options = { desc = "Next buffer"; }; }
    { mode = "n"; key = "[b"; action = "<cmd>bprevious<cr>"; options = { desc = "Prev buffer"; }; }
    { mode = "n"; key = "]b"; action = "<cmd>bnext<cr>"; options = { desc = "Next buffer"; }; }
    { mode = "n"; key = "<leader>bb"; action = "<cmd>e #<cr>"; options = { desc = "Switch to Other Buffer"; }; }
    { mode = "n"; key = "<leader>`"; action = "<cmd>e #<cr>"; options = { desc = "Switch to Other Buffer"; }; }
    # Clear search with <esc>
    { mode = [ "i" "n" ]; key = "<esc>"; action = "<cmd>noh<cr><esc>"; options = { desc = "Escape and clear hlsearch"; }; }
    # Save file
    { mode = [ "i" "x" "n" "s" ]; key = "<C-s>"; action = "<cmd>w<cr><esc>"; options = { desc = "Save file"; }; }
    # Better indenting
    { mode = "v"; key = "<"; action = "<gv"; }
    { mode = "v"; key = ">"; action = ">gv"; }
    # Formatting
    # { mode = [ "n" "v" ]; key = "<leader>cf"; lua = true; action = helpers.mkRaw "function() Util.format({ force = true }) end"; options = { desc = "Format"; }; }
    # Quit
    { mode = "n"; key = "<leader>qq"; action = "<cmd>qa<cr>"; options = { desc = "Quit all"; }; }
  ];
}
